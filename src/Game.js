import React from 'react';
import PropTypes from 'prop-types'
import Background from './Background'
import Questions from './Questions'
import { fetchQuestions } from './helpers'

import SidePanel from './SidePanel'

class Game extends React.Component {
  constructor () {
    super()

    this.state = {
      questions: [],
      answers: [],
      currentQuestion: {},
      currentQuestionNumber: 0
    }
    this.generateQuestion = this.generateQuestion.bind(this)
    this.fetchQuestions = this.fetchQuestions.bind(this)
    this.onSelect = this.onSelect.bind(this)
  }

  onSelect (answer) {
    console.log(answer)
    return event => {
      const {
        currentQuestions: {
          correctAnswer
        }
      }
      if (answer === correctAnswer) {
        if(currentQuestionNumber === 11) {
          window.alert('Wygrales')
        }
      } else {
        this.setState(prevState => ({
          currentQuestionNumber: prevState.currentQuestionNumber + 1
        }), this.generateQuestion)
        window.alert('Przegrałrześ')
      }
    }
  }

  generateQuestion () {
    const {
      questions,
      currentQuestionNumber
    } = this.state
    const currentQuestion = questions[currentQuestionNumber]
    const {
      correctAnswer,
      incorrectAnswers
    } = currentQuestion

    const answers = [ correctAnswer, ...incorrectAnswers ]
    this.setState ({
      answers,
      currentQuestion
    })
    console.log(currentQuestion)
  }

  fetchQuestions () {
    const {
        appSettings: {
            difficulty
        }
    } = this.props
    fetchQuestions(this.props.appSettings.difficulty)
    .then(questions => {
        this.setState({ questions }, this.generateQuestion)
    })
  }

  componentDidMount () {
    const {
        appSettings: {
            nickname,
            difficulty,
            gameStarted
        },
        history
    } = this.props
    if (gameStarted && difficulty && nickname) {
        this.fetchQuestions(difficulty)
    } else {
        history.replace('/')
    }
  }

  render() {
    const {
      currentQuestion: {
        question
      },
      answers
    } = this.state
    return (
      <div className='l-game'>
        <Background>
          <Questions
            onSelect 
            question={question}
            answers={answers}
          />
        </Background>
        <SidePanel />
      </div>
    )
  }
}

Game.propTypes = {
  appSettings: PropTypes.object,
  history: PropTypes.object
}

export default Game;
