import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch
} from 'react-router-dom'

import Setup from './Setup';
import Home from './Home';
import Game from './Game';

class App extends React.Component {
  constructor () {
    super()
    this.state = {
      nickname: '',
      difficulty: '',
      gameStarted: false
    }
    this.setupApp = this.setupApp.bind(this)
    this.setGameStarted = this.setGameStarted.bind(this)
  }

  setGameStarted () {
   // this.state.gameStarted = true
   this.setState({
    gameStarted: true
   })
  }

  setupApp (nickname, difficulty, callback) {
    this.setState({
      nickname,
      difficulty
    }, callback)
  }

  render() {
   return (
     <BrowserRouter>
      <Switch>
        <Route
          exact
          path='/'
          component={() => (
            <Home
              setGameStarted = {this.setGameStarted}
            />
          )}
        />
        <Route
          path='/setup'
          component={
            props => (
            <Setup
              {...props}
              gameStarted = { this.state.gameStarted }
              setupApp = { this.setupApp }
            />
          )}
        />
        <Route
          path='/game'
          component={ ({ history }) => (
            <Game
              history = { history }
              appSettings = { this.state }
            />
          ) }
        />
          <div className='App'>
          </div>
        </Switch>
      </BrowserRouter>
    )
  }
}

export default App;
