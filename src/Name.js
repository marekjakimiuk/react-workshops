import React, { Fragment } from 'react';
import PropTypes from 'prop-types'

class Name extends React.Component {
  render() {
    const {
      name,
      lastName,
      age
    } = this.props
    return (
      <Fragment>
        <div className='l-centered'>
          { name }
          { lastName }
          { age }
        </div>
      </Fragment>
    )
  }
}

Name.propTypes = {
  name: PropTypes.string,
  lastName: PropTypes.string,
  age: PropTypes.number
}


Name.defaultProps = {
  name: 'Podaj imie',
  lastName: 'Podaj nazwisko',
  age: 15
}


export default Name;
