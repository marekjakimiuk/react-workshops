import React from 'react';
import PropTypes from 'prop-types'


class Questions extends React.Component {
  constructor (){
    super ()
    this.renderItem = this.renderItem.bind(this)
  }
  renderItem (answer,index, originalArray) {
    return <li
      className='c-question'
      key={`answer-${answer}`}
      onClick={ this.props.onSelect(answer)}
      >
      <span className='c-question__label'>
        {String.fromCharCode(65 + index)}  {answer}
        </span>
      </li>
  }

  render () {
    return (
      <div className='c-questions'>
        <p className='c-questions__title c-question'>
          { this.props.question }
        </p>
        <ul className='c-questions__list'>
          { this.props.answers.map(this.renderItem) }
        </ul>
      </div>
    )
  }


}

Questions.propTypes = {
  question: PropTypes.string,
  answers: PropTypes.array,
  onSelect: PropTypes.func
}

export default Questions;
