import React from 'react';
import PropTypes from 'prop-types'

class Setup extends React.Component {
  constructor () {
    super()
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange (e) {
    const {
      value
    } = e.target
    this.setState((() => {
      return {
        nickname: value
      }
    }))
  }

  componentDidMount (){
    const {
      history,
      gameStarted
    } = this.props
  }

  onSubmit (e) {
    e.preventDefault()
    const {
      nickname: {
        value: nicknameValue
      },
      difficulty: {
        value: difficultyValue
      }
    } = this.refs

    if (!nicknameValue) {
      window.alert('nick nie moze byc pusty')
      return
    }
    this.props.setupApp(nicknameValue, difficultyValue, () => { this.props.history.push('/game')} )
  }

  render() {
    return (
      <div className='l-centered'>
        <form
          className='f-start'
          onSubmit={this.onSubmit}
        >
          <label
            htmlFor='nickname'
            className='f-start__label'
          >
            Name:
          </label>
          <input
            className='f-start__control'
            type='text'
            ref='nickname'
            id='nickname'
            autoFocus
          />

          <label
            htmlFor='difficulty'
            className='f-start__label'
          >
            Difficulty
          </label>
          <select
            className='f-start__control'
            id='difficulty'
            ref='difficulty'
          >
            <option value='easy'>Easy</option>
            <option value='medium'>Medium</option>
            <option value='hard'>Hard</option>
          </select>
          <button
            className="f-start__action"
            type='submit'
          >
            Wyslij
          </button>
        </form>
      </div>
    )
  }
}

Setup.propTypes = {
  name: PropTypes.string,
  history: PropTypes.object,
  gameStarted: PropTypes.bool
}

Setup.defaultProps = {
  name: 'Podaj imie'
}

export default Setup;
